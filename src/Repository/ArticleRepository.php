<?php

namespace App\Repository;

use App\Entity\Article;

class ArticleRepository
{

    private $connection;

    public function __construct()
    {
        try {
            $this->connection = new \PDO(
                "mysql:host=" . getenv("MYSQL_HOST") . ":3306;dbname=" . getenv("MYSQL_DATABASE"),
                getenv("MYSQL_USER"),
                getenv("MYSQL_PASSWORD")
            );

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {
            dump($e);
        }
    }

    private function fetch(string $query, array $params = [])
    {
        try {
            dump($this->connection);
            $query = $this->connection->prepare($query);

            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }

            $query->execute();

            $result = [];
            foreach ($query->fetchAll() as $row) {
                $result[] = Article::fromSQL($row);
            }

            if (count($result) <= 1) {
                return $result[0];
            }

            return $result;

        } catch (\PDOException $e) {
            dump($e);
        }
    }

    public function getAll()
    {
        return $this->fetch("SELECT * FROM db_article");
    }

    public function get(int $id)
    {

        return $this->fetch("SELECT * FROM db_article WHERE id=:id", [":id" => $id]);
    }

    public function add(Article $article)
    {
        $this->fetch(
            "INSERT INTO db_article (title, lead_paragraph, article_content, date)  VALUES (:title, :lead_paragraph, :article_content, :date)",
            [
                ":title" => $article->title,
                ":lead_paragraph" => $article->leadParagraph,
                ":article_content" => $article->articleContent,
                ":date" => $article->date->format("Y-m-d"),
            ]
        );

        $article->id = intval($this->connection->lastInsertId());
        return $article;
    }

    public function update(Article $article)
    {
        return $this->fetch(
            "UPDATE db_article SET title=:title, lead_paragraph=:lead_paragraph, article_content=:article_content, date=:date WHERE id=:id",
            [
                ":title" => $article->title,
                ":lead_paragraph" => $article->leadParagraph,
                ":article_content" => $article->articleContent,
                ":date" => $article->date->format("Y-m-d"),
                ":id" => $article->id
            ]);
    }

    public function delete(int $id){
        return $this->fetch("DELETE FROM db_article WHERE id=:id",
    [
        ":id" => $id
    ]);
    }
}