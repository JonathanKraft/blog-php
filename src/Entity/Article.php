<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints\DateTime;

class Article
{
  public $title;
  public $leadParagraph;
  public $articleContent;
  public $date;
  public $id;

  public function __construct(string $title = null, string $leadParagraph = null, string $articleContent = null, \DateTime $date = null, int $id = null)
  {
    $this->title = $title;
    $this->leadParagraph = $leadParagraph;
    $this->articleContent = $articleContent;
    $this->date = $date;
    $this->id = $id;
  }

  public static function fromSQL(array $rawData)
  {
    return new Article(
      $rawData["title"],
      $rawData["lead_paragraph"],
      $rawData["article_content"],
      new \DateTime($rawData["date"]),
      $rawData["id"]
    );
  }

}