<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;


/**
 * @Route("/")
 */
class HomeController extends AbstractController
{

  /**
   * @Route("/", name="home")
   */
  public function index(ArticleRepository $repo)
  {
    $result = $repo->getAll();
    return $this->render('home.html.twig', [
      'result' => $result
    ]);
  }

  /**
   * @Route("/aboutme", name="aboutme")
   */
  public function aboutme()
  {
    $title = "title";
    return $this->render("aboutme.html.twig", [
      "title" => $title
    ]);
  }

  /**
   * @Route("/contact", name="contactme")
   */
  public function contact()
  {
    $title = "title";
    return $this->render("contact.html.twig", [
      "title" => $title
    ]);
  }
}