<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class ModifyArticleController extends Controller
{
    /**
     * @Route("/modify-article/{id}", name="modify-article")
     */
    public function index(int $id, ArticleRepository $repo, Request $request)
    {
        $article = $repo->get($id);

        $form = $this->createForm(ArticleType::class, $article);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $repo->update($form->getData());
           return $this->redirectToRoute("home");
        }

        return $this->render('modify-article/index.html.twig', [
           "form" => $form->createView(),
           "article" => $article
        ]);
    }

    /**
     * @Route("/remove-article/{id}", name="remove-article")
     */
    public function removeArticle(int $id, ArticleRepository $repo){
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }
}
