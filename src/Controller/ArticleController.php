<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;

  /**
   * @Route("/article")
   */
class ArticleController extends AbstractController {

  /**
   * @Route("/{id}", name="article")
   */
  public function index(int $id, ArticleRepository $repo){

    $result = $repo->get($id);
    return $this->render('article.html.twig', [
      'result' => $result
    ]);
  }

    /**
     * @Route("/article/remove/{id}", name="remove-article")
     */
    public function remove(int $id, ArticleRepository $repo) {
      $repo->delete($id);
      return $this->redirectToRoute("home");
  }

}