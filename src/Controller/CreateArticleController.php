<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;

  /**
   * @Route("/create-article")
   */
 class CreateArticleController extends AbstractController {

   /**
    * @Route("/", name="create-article")
    */
  public function index(Request $request, ArticleRepository $repo){

    $form = $this->createForm(ArticleType::class);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $repo->add($form->getData());

      return $this->redirectToRoute("home");
    }

    return $this->render("create-article.html.twig", [
      "form" => $form->createView()
    ]);
  }
}